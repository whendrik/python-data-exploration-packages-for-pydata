# Presentation & test notebooks for PYDATA EINDHOVEN 2021 - NOVEMBER 12TH, 2021

## Presentation as `.pdf`

[data_exploration.pdf](data_exploration.pdf)

## Test Notebooks & Environment

With exception of `4 - dTreeViz.ipynb` - which needs graphviz - all notebooks will run on most jupyter environments. Tested on November 2021 with a docker environment created by `run_jupyter.sh`; and artifacts;

- `SWEETVIZ_REPORT.html` from SweetViz
- `pandas_profiling_report.html` from Pandas Profiling
- `DTreeViz_384.svg` from DtreeViz

## Shortlist of tools that didn't made it in final top-5

- Pandasgui
- bamboolib
- missingno (missing values, nice and easy)
- https://erroranalysis.ai/
- MiTo
- Pandas / Sns / Matplotlib
- https://github.com/lux-org/lux
